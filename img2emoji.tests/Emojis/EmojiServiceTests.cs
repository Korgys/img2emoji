﻿using img2emoji.Emojis;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using img2emoji.Images;
using Image = img2emoji.Images.Image;

namespace img2emoji.tests.Emojis;

[TestClass]
public class EmojiServiceTests
{
    private EmojiService _emojiService;
    private Image<Rgba32> _testImage;
    private Dimension _dimension;

    [TestInitialize]
    public void Setup()
    {
        _emojiService = new EmojiService();
        _dimension = new Dimension { Largeur = 2, Hauteur = 2 };
        _testImage = new Image<Rgba32>(2, 2);
        _testImage[0, 0] = new Rgba32(0, 0, 0); // noir
        _testImage[0, 1] = new Rgba32(255, 255, 255);  // blanc
        _testImage[1, 0] = new Rgba32(255, 0, 0); // rouge
        _testImage[1, 1] = new Rgba32(0, 255, 0);  // vert
    }

    [TestMethod]
    public void TransformeImageEnEmojis_RenvoieLesBonsEmojis()
    {
        // Carré noir blanc rouge vert
        var attendu = $"⬛️\U0001f7e5{Environment.NewLine}⬜️\U0001f7e9{Environment.NewLine}";
        var resultat = _emojiService.TransformeImageEnEmojis(
            new Image { Contenu = _testImage, Dimension = _dimension });
        Assert.AreEqual(attendu, resultat);
    }

    [TestMethod]
    public void ConvertirCouleurVersEmoji_RenvoieLesBonnesCouleursEmojis()
    {
        var noir = new Rgba32(0, 0, 0);
        var resultat = _emojiService.ConvertirCouleurVersEmoji(noir);
        Assert.AreEqual("⬛️", resultat);
    }

    [TestMethod]
    public void DistanceCouleur_CalculeLaDistanceCorrecte()
    {
        var couleur1 = new Rgba32(10, 10, 10);
        var couleur2 = new Rgba32(20, 20, 20);

        var result = _emojiService.ObtenirDistanceCouleur(couleur1, couleur2);
        Assert.AreEqual(Math.Sqrt(300), result, 0.001);
    }
}
