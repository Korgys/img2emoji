using System.IO.Abstractions;
using img2emoji.Images;
using Moq;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;

namespace img2emoji.Tests.Images;

[TestClass]
public class ServiceImageTests
{
    private Mock<IFileSystem> _mockSystemeFichiers;
    private ImageService _imageService;

    [TestInitialize]
    public void Initialiser()
    {
        _mockSystemeFichiers = new Mock<IFileSystem>();
        // Permet d'ignorer l'erreur NullRefException dans le constructeur li� � Directory.GetCurrentDirectory()
        _mockSystemeFichiers.Setup(fs => fs.Directory.GetCurrentDirectory()).Returns("");
        _imageService = new ImageService(_mockSystemeFichiers.Object);
    }

    [TestMethod]
    public void ObtenirImage_RetourneImageSiExiste()
    {
        var nomImage = "pepe";
        var cheminComplet = "../../../../img2emoji/Ressources/pepe.png";
        _mockSystemeFichiers.Setup(fs => fs.Path.Combine(It.IsAny<string>(), It.IsAny<string>())).Returns(cheminComplet);
        _mockSystemeFichiers.Setup(fs => fs.File.Exists(It.IsAny<string>())).Returns(true);

        var resultat = _imageService.ObtenirImage(nomImage);

        Assert.IsNotNull(resultat);
        Assert.IsNotNull(resultat.Contenu);
        _mockSystemeFichiers.Verify(fs => fs.File.Exists(It.Is<string>(s => s == cheminComplet)), Times.Once());
    }

    [TestMethod]
    public void ObtenirImage_RetourneImageAleatoireSiNomImageNonTrouve()
    {
        var nomImage = "inexistant";
        _mockSystemeFichiers.Setup(fs => fs.Path.Combine(It.IsAny<string>(), It.IsAny<string>())).Returns("");
        _mockSystemeFichiers.Setup(fs => fs.File.Exists(It.IsAny<string>())).Returns(false);
        _mockSystemeFichiers.Setup(fs => fs.Directory.GetFiles(It.IsAny<string>(), "*.*"))
            .Returns(new[] { "../../../../img2emoji/Ressources/pepe.png" });

        var resultat = _imageService.ObtenirImage(nomImage);

        Assert.IsNotNull(resultat);
        Assert.IsNotNull(resultat.Contenu);
    }

    [TestMethod]
    public void ObtenirImage_LanceExceptionSiAucuneImageTrouvee()
    {
        var nomImage = "inexistant";
        _mockSystemeFichiers.Setup(fs => fs.Path.Combine(It.IsAny<string>(), It.IsAny<string>())).Returns("");
        _mockSystemeFichiers.Setup(fs => fs.File.Exists(It.IsAny<string>())).Returns(false);
        _mockSystemeFichiers.Setup(fs => fs.Directory.GetFiles(It.IsAny<string>(), "*.*")).Returns(new string[] { });

        var resultat = _imageService.ObtenirImage(nomImage);

        Assert.IsNull(resultat);
    }

    [TestMethod]
    public void ObtenirDimension_AvecImageValide_RetourneDimensionsCorrectes()
    {
        int largeurAttendue = 50;
        int hauteurAttendue = 60;
        using var image = new Image<Rgba32>(100, 50); // Image avec un ratio de 2:1

        var dimension = _imageService.ObtenirDimension(image);

        Assert.AreEqual(hauteurAttendue, dimension.Hauteur);
        Assert.AreEqual(largeurAttendue, dimension.Largeur);
    }

    [TestMethod]
    public void ObtenirDimension_AvecImageValide_RetourneDimensionsPourRatioInverse()
    {
        int largeurAttendue = 15;
        int hauteurAttendue = 50;
        using var image = new Image<Rgba32>(50, 100); // Image avec un ratio de 1:2

        var dimension = _imageService.ObtenirDimension(image);

        Assert.AreEqual(hauteurAttendue, dimension.Hauteur);
        Assert.AreEqual(largeurAttendue, dimension.Largeur);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ObtenirDimension_AvecImageNulle_LanceArgumentNullException()
    {
        var dimension = _imageService.ObtenirDimension(null);
    }
}
