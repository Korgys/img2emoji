# Image de base officielle de Microsoft pour .NET
FROM mcr.microsoft.com/dotnet/runtime:8.0 AS base
WORKDIR /app

# Définit l'image pour construire le projet
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["img2emoji/img2emoji.csproj", "img2emoji/"]
RUN dotnet restore "img2emoji/img2emoji.csproj"
COPY . .
WORKDIR "/src/img2emoji"
RUN dotnet build "img2emoji.csproj" -c Release -o /app/build

# Publie l'application
FROM build AS publish
RUN dotnet publish "img2emoji.csproj" -c Release -o /app/publish

# Crée l'image finale
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "img2emoji.dll"]
