﻿using img2emoji.Emojis;
using img2emoji.Images;
using System.IO.Abstractions;

namespace img2emoji;

public class Program
{
    public static void Main(string[] args)
    {
        var imageService = new ImageService(new FileSystem());
        var emojiService = new EmojiService();

        var imageNom = ObtenirNomImageDepuisArguments(args);
        var imageSelectionne = imageService.ObtenirImage(imageNom);

        if (imageSelectionne == null)
        {
            Console.WriteLine("Aucune image valide n'a été trouvée.");
            return;
        }

        imageSelectionne.Dimension = imageService.ObtenirDimension(imageSelectionne.Contenu);

        var texteTransforme = emojiService.TransformeImageEnEmojis(imageSelectionne);
        Console.WriteLine(imageSelectionne.Nom);
        Console.WriteLine(texteTransforme);
    }

    private static string ObtenirNomImageDepuisArguments(string[] args)
    {
        return args != null && args.Length > 0 ? args[0] : "";
    }
}