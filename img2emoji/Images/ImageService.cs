﻿using SixLabors.ImageSharp.PixelFormats;
using System.IO.Abstractions;
using ImageSharp = SixLabors.ImageSharp.Image;

namespace img2emoji.Images;

public class ImageService : IImageService
{
    private readonly string _cheminAcces;
    private readonly string[] _formatImagesAcceptes = { "png", "jpeg", "jpg" };
    private readonly IFileSystem _fileSystem;

    public ImageService(IFileSystem fileSystem)
    {
        _fileSystem = fileSystem;
        _cheminAcces = Path.Combine(_fileSystem.Directory.GetCurrentDirectory(), "Ressources");
    }

    public Image ObtenirImage(string nomImage)
    {
        foreach (var format in _formatImagesAcceptes)
        {
            var cheminImage = _fileSystem.Path.Combine(_cheminAcces, $"{nomImage}.{format}");
            if (_fileSystem.File.Exists(cheminImage))
            {
                return new Image 
                { 
                    Contenu = ImageSharp.Load<Rgba32>(cheminImage) 
                };
            }
        }

        var fichiersImages = _fileSystem.Directory.GetFiles(_cheminAcces, "*.*")
            .Where(f => _formatImagesAcceptes.Any(ext => f.EndsWith($".{ext}")))
            .ToArray();

        if (fichiersImages.Length > 0)
        {
            var cheminImageAleatoire = fichiersImages[new Random().Next(fichiersImages.Length)];
            return new Image
            {
                Nom = Path.GetFileName(cheminImageAleatoire),
                Contenu = ImageSharp.Load<Rgba32>(cheminImageAleatoire) 
            };
        }

        return null;
    }

    public Dimension ObtenirDimension(ImageSharp image)
    {
        if (image == null)
            throw new ArgumentNullException(nameof(image), "L'image fournie est nulle.");

        double ratio = (double)image.Width / image.Height * 0.6;
        int dimensionMax = 50;
        int hauteur, largeur;
        
        if (image.Height > image.Width) {
            hauteur = dimensionMax;
            largeur = (int)(hauteur * ratio);
        } else {
            largeur = dimensionMax;
            hauteur = (int)(largeur * ratio);
        }

        return new Dimension { Hauteur = hauteur, Largeur = largeur };
    }

}
