﻿using ImageSharp = SixLabors.ImageSharp.Image;

namespace img2emoji.Images;

public interface IImageService
{
    Image ObtenirImage(string nomImage);
    Dimension ObtenirDimension(ImageSharp image);
}
