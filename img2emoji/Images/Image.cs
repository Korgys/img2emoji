﻿using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;

namespace img2emoji.Images;

public class Image
{
    public string Nom { get; set; }
    public Image<Rgba32> Contenu { get; set; }
    public Dimension Dimension { get; set; }
}
