﻿namespace img2emoji.Images;

public class Dimension
{
    public int Largeur { get; set; }
    public int Hauteur { get; set; }
}
