﻿using img2emoji.Images;
using SixLabors.ImageSharp.PixelFormats;

namespace img2emoji.Emojis;

public interface IEmojiService
{
    string TransformeImageEnEmojis(Image image);
    string ConvertirCouleurVersEmoji(Rgba32 couleur);
    double ObtenirDistanceCouleur(Rgba32 couleur1, Rgba32 couleur2);
}
