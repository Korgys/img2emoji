﻿using img2emoji.Images;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System.Text;
using SixLabors.ImageSharp.Advanced;

namespace img2emoji.Emojis;

public class EmojiService : IEmojiService
{
    public string TransformeImageEnEmojis(Image image)
    {
        // Préparer le StringBuilder pour contenir la sortie
        var sb = new StringBuilder();

        // Redimensionner l'image selon les dimensions spécifiées
        image.Contenu.Mutate(x => x.Resize(image.Dimension.Largeur, image.Dimension.Hauteur));

        var pixelMemoryGroup = image.Contenu.GetPixelMemoryGroup();
        foreach (var memory in pixelMemoryGroup)
        {
            Span<Rgba32> pixelSpan = memory.Span; // Obtenez le span de chaque segment
            for (int y = 0; y < image.Dimension.Hauteur; y++)
            {
                for (int x = 0; x < image.Dimension.Largeur; x++)
                {
                    var pixel = pixelSpan[y * image.Dimension.Largeur + x]; // Accéder au pixel
                    sb.Append(ConvertirCouleurVersEmoji(pixel)); // Convertir le pixel en emoji
                }
                sb.AppendLine(); // Nouvelle ligne après chaque ligne de pixels
            }
        }

        return sb.ToString();
    }

    public string ConvertirCouleurVersEmoji(Rgba32 couleur)
    {
        // Définir une liste de couleurs de référence pour les emojis
        var couleursEmojis = new Dictionary<string, Rgba32>
        {
            ["⬛️"] = new Rgba32(10, 10, 10),       // Noir
            ["⬜️"] = new Rgba32(240, 240, 240), // Blanc
            ["🟥"] = new Rgba32(200, 0, 0),     // Rouge
            ["🟧"] = new Rgba32(255, 123, 63),   // Orange
            ["🟨"] = new Rgba32(255, 186, 73),   // Jaune
            ["🟩"] = new Rgba32(0, 128, 0),     // Vert
            ["🟩"] = new Rgba32(104, 152, 76),     // Vert
            ["🟦"] = new Rgba32(0, 0, 200),     // Bleu
            ["🟪"] = new Rgba32(128, 0, 128),   // Violetclea
            ["🟫"] = new Rgba32(177, 124, 105),    // Brun
            ["🟫"] = new Rgba32(142, 110, 95),    // Brun
            ["🚽"] = new Rgba32(128, 128, 128)    // Gris
        };

        // Calculer la distance entre la couleur donnée et chaque couleur de référence
        var emojiProche = couleursEmojis
            .OrderBy(c => ObtenirDistanceCouleur(c.Value, couleur))
            .First().Key;

        return emojiProche;
    }

    // Fonction pour calculer la distance Euclidienne entre deux couleurs
    public double ObtenirDistanceCouleur(Rgba32 c1, Rgba32 c2)
    {
        return Math.Sqrt(
            Math.Pow(c1.R - c2.R, 2) 
            + Math.Pow(c1.G - c2.G, 2) 
            + Math.Pow(c1.B - c2.B, 2));
    }
}
