# img2emoji

## Description

img2emoji est une application console en .NET qui transforme des images en représentations en emojis. Le projet utilise ImageSharp pour manipuler les images et convertit les pixels de l'image en emojis basés sur la couleur la plus proche.

## Prérequis

Pour exécuter ou développer ce projet, vous aurez besoin de :

.NET 6.0 SDK ou une version plus récente.
Docker (pour le déploiement via Docker).
Un éditeur ou IDE tel que Visual Studio ou VSCode.

## Installation

Clonez le dépôt GitLab ou GitHub à l'aide de la commande suivante :

```bash
git clone https://gitlab.com/Korgys/img2emoji.git
```

Naviguez dans le dossier du projet cloné :

```bash
cd img2emoji
```

Restaurez les dépendances du projet :

```bash
dotnet restore
```

## Utilisation

Pour exécuter l'application en utilisant le SDK .NET, utilisez la commande suivante dans le répertoire principal du projet :

```bash
dotnet run --project img2emoji
```

Exemple :

```bash
> dotnet run

🟨🟨🟫🟫🟫🟫🟩🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🚽🚽🚽🚽🚽🚽🚽🚽
🟫🟫🟫🟫🟫🟩🟩🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🚽🚽🚽🚽🟩🟩🟩🟩
🟫🟫🟫🟫🟫🟩🟩🟩🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🚽🚽🚽🟩🟩🟩🟩🟩
🟫🟫🟫🟫🟫🟩🟩🟩🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🚽🚽🟩🟩🟩🟩🟩
🟫🟫🟫🟫⬛️🟫🟩🟩🟩🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🟩🟩🟩🟩🟩🟩🟩
🟫🟫🟫🟩🟩🟫🟨🟨🟫🟩🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🟩🟩🟩🟩🟩🟫🟫
🟫🟫🟫🟩🟩🟩🟨🟨🟨🟨🟧🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟩🟩🟩🟫🟨🟨🟨🟥
🟫🟫🟫🟩🟩🟩🟫🟨🟨🟨🟨🟨🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫🟨🟨🟨🟨🟨🟫⬛️
🟫🟫🟩🟩🟩🟩🟩🟫🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟩🟩
🟫🟫🟩🟩🟩🟩🟩⬛️🟫🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟩🟩🟩
🟫🟩⬛️⬛️⬛️⬛️🟩⬛️⬛️🟫🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟫🟩🟩🟩🟩
🟫⬛️⬛️⬛️⬛️⬛️🟩🟩🟩🟨🟨🟨🟫🟨🟨🟨🟨🟨🟨🟨🟧🟧🟨🟨🟨🟩🟩🟩🟩🟩
🟫⬛️⬛️⬛️⬛️⬛️🟩🟩🟫🟨🟨🟫🟫🟧🟨🟨🟨🟨🟨🟨🟫🟫🟨🟨🟨🟧🟩🟩🟩🟩
⬛️⬛️⬛️⬛️⬛️⬛️⬛️🟩🟨🟨🟨🟨🟧🟨🟨🟧🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟩🟩🟩🟩
⬛️⬛️⬛️⬛️⬛️⬛️🟩🟩🟧🟧🟧🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟧🟧🟧🟨🟫🟩🟩🟩
🟩🟩🟩🟩🟩🟩🟩🟩🟧🟧🟧🟨🟨🟨🟨🟧🟧🟧🟨🟨🟨🟨🟧🟧🟧🟨🟨🟩🟩🟩
🟩🟩🟩🟩🟩🟩🟩🟩🟨🟨🟨🟨🟨🟨🟨🟧🟧🟧🟨🟨🟨🟨🟨🟨🟨🟨🟨🟩🟩🟩
🟩🟩🟩🟩🟩🟩🟩🟩🟫🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟩🟩🟩
🟩🟩🟩🟩🟩🟩🟩🟩🟩🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟫🟩🟩
🟩🟩🟩🟩🟩🟩🟩🟩🟩🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟧🟩🟩
```

La taille de l'image est définie par la variable `dimensionMax` dans `ImageService.cs`.

## Arguments

L'application peut être lancée avec des arguments pour spécifier le nom de l'image à transformer :

```bash
dotnet run --project img2emoji "pepe"
```

Si aucun argument n'est lancé, l'application ira chercher une image au hasard dans le répertoire `img2emoji/Ressource`.

Il est possible d'ajouter vos propres images dans le répertoire, il suffira alors de recompiler le programme pour les voir apparaitre.

Seuls les formats `.png` `.jpg` et `.jpeg` sont acceptés pour le moment.

## Déploiement avec Docker

Pour construire l'image Docker et l'exécuter, utilisez les commandes suivantes :

```bash
docker build -t img2emoji .
docker run img2emoji
```

## Contribution

Les contributions à ce projet sont les bienvenues. Veuillez suivre les pratiques standard pour les contributions :

- Fork le projet.
- Créez une branche pour chaque fonctionnalité ou correction.
- Faites un pull request pour merger votre branche dans la branche principale après avoir testé votre code.

## Licence

Ce projet est distribué à titre gratuit et pédagogique dans le cadre de cours d'informatique à l'initiation DevOps.
